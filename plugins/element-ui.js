/* eslint-disable import/no-extraneous-dependencies */
import Vue from 'vue';
/* eslint-enable */
import Element from 'element-ui';
import locale from 'element-ui/lib/locale/lang/en';

Vue.use(Element, { locale });
